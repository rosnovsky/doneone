# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/rosnovsky/doneone/compare/v0.0.1...v0.1.0) (2023-11-05)


### ⚠ BREAKING CHANGES

* **cli:** task id is now required

closes !5

### Features

* **cli:** add id to tasks ([f246e61](https://gitlab.com/rosnovsky/doneone/commit/f246e61e1dc42f54f1e110c27ccb956fe3e3452a))

### 0.0.1 (2023-11-05)


### Features

* initial commit ([b242db3](https://gitlab.com/rosnovsky/doneone/commit/b242db31acae640d5db527fe514f5849d06c6719))
