import fs from "fs";
import path from "path";
import os from "os";
import matter from "gray-matter";
import { TaskDetails } from "../types";


const saveTaskToFile = (taskDetails: TaskDetails) => {
  const { title, dueDate, priority, labels, description } = taskDetails;
  const tasks = [] as any[];
  const homeDirectory = os.homedir();
  const tasksDirectory = path.join(
    homeDirectory,
    "Documents",
    "Doneone",
    "tasks"
  );
  const files = fs.readdirSync(tasksDirectory);

  for (const file of files) {
    if (path.extname(file) === ".md") {
      const filePath = path.join(tasksDirectory, file);
      const content = fs.readFileSync(filePath, "utf8");
      const parsedContent = matter(content);
      if (parsedContent.data.status !== "complete") {
        tasks.push({
          ...parsedContent.data
        });
      }
    }
  }
  const totalTasks: number = tasks.length
  const newTaskId = totalTasks + 1;
  const labelsString = labels.map((label) => `"${label}"`).join(", ");

  const frontmatter = `---
id: "${newTaskId}"
title: "${title.replace(/"/g, '\\"')}"
dueDate: "${dueDate}"
priority: "${priority}"
labels: [${labelsString}]
description: "${description}"
---
`;

  const slug = title
    .toLowerCase()
    .replace(/[^a-z0-9]+/g, "-")
    .replace(/(^-|-$)/g, "");
  const fileName = `${slug}.md`;

  if (!fs.existsSync(tasksDirectory)) {
    fs.mkdirSync(tasksDirectory, { recursive: true });
  }

  const filePath = path.join(tasksDirectory, fileName);

  fs.writeFile(filePath, frontmatter, (err) => {
    if (err) {
      console.error("Error writing task to file:", err);
    } else {
      console.log(`Task saved to ${filePath}`);
    }
  });
};

export default saveTaskToFile;
