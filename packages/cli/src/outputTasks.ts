import chalk from "chalk";
import fs from "fs";
import path from "path";
import os from "os";
import matter from "gray-matter";
import { TaskDetails } from "../types";

const getPriorityMarker = (priority: string) => {
  switch (priority) {
    case "High":
      return chalk.red.bold("!!!");
    case "Medium":
      return chalk.yellow.bold("!! ");
    case "Low":
      return chalk.green.bold("!  ");
    default:
      return chalk.gray.bold("");
  }
};

const displayTasks = () => {
  const homeDirectory = os.homedir();
  const tasksDirectory = path.join(
    homeDirectory,
    "Documents",
    "Doneone",
    "tasks"
  );
  const files = fs.readdirSync(tasksDirectory);
  const tasks = [] as any[];

  for (const file of files) {
    if (path.extname(file) === ".md") {
      const filePath = path.join(tasksDirectory, file);
      const content = fs.readFileSync(filePath, "utf8");
      const parsedContent = matter(content);
      if (parsedContent.data.status !== "complete") {
        tasks.push({
          ...parsedContent.data
        });
      }
    }
  }

  const tasksByDueDate = tasks.reduce((acc, task) => {
    const dueDate = task.dueDate || "No Due Date";
    if (!acc[dueDate]) {
      acc[dueDate] = [];
    }
    acc[dueDate].push(task);
    return acc;
  }, {});

  const today = new Date();
  const todayStr = today.toISOString().split("T")[0];
  const dueToday = tasksByDueDate[todayStr] || [];

  console.log(chalk.bold(`Total tasks: ${tasks.length}`));
  console.log(chalk.bold(`Due today: ${dueToday.length}`));

  if (dueToday.length > 0) {
    console.log(
      chalk.bold.underline(
        todayStr + " " + today.toLocaleString("en-US", { weekday: "long" })
      )
    );
    dueToday.forEach((task: TaskDetails) => {
      console.log(
        `${getPriorityMarker(task.priority)} [${chalk.dim(task.id)}] ${chalk.bold(
          task.title
        )} ${task.labels.map((label) => chalk.cyan(`#${label}`)).join(" ")}`
      );
      console.log(chalk.italic(task.description || ""));
      console.log(chalk.gray("\n---\n"));
    });
  }

  console.log(chalk.bold("Due later"));
  Object.keys(tasksByDueDate)
    .sort()
    .forEach((dueDate) => {
      if (dueDate !== todayStr) {
        tasksByDueDate[dueDate].forEach((task: TaskDetails) => {
          console.log(
            `${dueDate} ${getPriorityMarker(task.priority)} [${chalk.dim(task.id)}] ${chalk.bold(
              task.title
            )} ${task.labels.map((label) => chalk.cyan(`#${label}`)).join(" ")}`
          );
          console.log(chalk.italic(task.description || ""));
          console.log(chalk.gray("\n---\n"));
        });
      }
    });
};

export default displayTasks;
