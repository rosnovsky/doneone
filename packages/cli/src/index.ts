#!/usr/bin/env node

import { Command } from "commander";
import saveTaskToFile from "./saveTaskToFile.js";
import displayTasks from "./outputTasks.js";

const program = new Command();

program
  .name("DoneOne CLI")
  .description("CLI to manage your todos")
  .version("0.1.0");

program
  .command("add")
  .description("Add a new todo task")
  .action(async () => {
    const inquirer = await import("inquirer");
    console.log("Adding a new task...");
    const questions = [
      {
        type: "input",
        name: "title",
        message: "What's the title of your task?",
        validate: (input: string) => !!input || "A title is required."
      },
      {
        type: "input",
        name: "dueDate",
        message: "Enter the due date (YYYY-MM-DD):",
        validate: (input: string) => {
          if (!input) return true;
          const [year, month, day] = input.split("-").map(Number);
          const inputDate = new Date(year, month - 1, day);
          const today = new Date();
          const todayDateOnly = new Date(
            today.getFullYear(),
            today.getMonth(),
            today.getDate()
          );
          if (inputDate < todayDateOnly) {
            return "Due date must not be before today’s date.";
          }
          return true;
        }
      },
      {
        type: "list",
        name: "priority",
        message: "Select the priority for the task:",
        choices: ["High", "Medium", "Low"],
        default: "Medium"
      },
      {
        type: "checkbox",
        name: "labels",
        message: "Select labels (tags) for the task:",
        choices: ["work", "personal", "drive", "email", "phone"]
      },
      {
        type: "input",
        name: "description",
        message: "Provide a detailed description for the task (optional):",
        default: ""
      }
    ];

    const answers = await inquirer.default.prompt(questions);
    saveTaskToFile(answers);
  });
program
  .command("list")
  .description("List all todo tasks")
  .action(() => {
    displayTasks();
  });

program.parse(process.argv);
