export interface TaskDetails {
  id: string;
  title: string;
  dueDate?: string;
  priority: "High" | "Medium" | "Low";
  labels: string[];
  description: string;
  status?: string;
}
